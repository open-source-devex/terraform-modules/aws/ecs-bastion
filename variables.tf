variable "name" {
  description = "The name for the bastion, used in all related resources"
  type        = string
}

variable "tags" {
  description = "Tags appended to all resources that will take them"
  type        = map(string)
  default     = {}
}

variable "aws_region" {
  description = "The AWS region where the ECS cluster lives"
  type        = string
}

variable "vpc_id" {
  description = "The ID of the VPC where the ECS task will be launched"
  type        = string
}

variable ecs_network_mode {
  description = "The network mode for the ECS task"
  type        = string
  default     = "awsvpc"
}

variable "ecs_launch_type" {
  description = "The launch type for the ECS task"
  type        = string
  default     = "FARGATE"
}

variable "bastion_ingress_cidr_blocks" {
  description = "The CIDR blocks used for the security group rule that allows SSH traffic to the bastion"
  type        = string
  default     = "0.0.0.0/0"
}

variable "bastion_egress_cidr_blocks" {
  description = "The CIDR blocks used for the security group rule that allows SSH traffic out from the bastion (where can you hop on next)"
  type        = string
}

variable "bastion_container_image" {
  description = "The container image for the bastion"
  type        = string
  default     = "registry.gitlab.com/open-source-devex/containers/bastion"
}

variable "bastion_container_task_cpu" {
  default = "256"
}

variable "bastion_container_task_memory" {
  default = "512"
}

variable "bastion_container_memory_reservation" {
  default = "64"
}

variable "bastion_encrypt_logs" {
  description = "Whether to encrypt the logs sent to cloudwatch with a CMK"
  type        = bool
  default     = false
}
