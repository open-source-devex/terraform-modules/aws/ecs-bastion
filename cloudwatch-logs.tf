resource "aws_cloudwatch_log_group" "bastion" {
  name = "${var.name}-bastion"
  tags = var.tags

  kms_key_id = module.bastion_logs.key_arn

  retention_in_days = 14
}

module "bastion_logs" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/kms-key.git?ref=v1.0.3"

  key_name        = "${var.name}-log"
  key_description = "CMK for ECS Bastion ${var.name} logs"

  create_key = var.bastion_encrypt_logs

  key_policy = [{
    effect = "Allow"
    principals = [{
      type        = "Service",
      identifiers = ["logs.${var.aws_region}.amazonaws.com"]
    }]
    actions = [
      "kms:Encrypt*",
      "kms:Decrypt*",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:Describe*"
    ]
    resources = ["*"]
    condition = []
  }]

  tags = var.tags
}
