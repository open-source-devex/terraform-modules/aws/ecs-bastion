#!/usr/bin/env sh

set -ev

VERSION_FILE="version.txt"

VERSION=$( cat ${VERSION_FILE} )
git tag --annotate v${VERSION} --message "Release v${VERSION}"
git push --tags

if [[ -z "$( git branch | grep '^* master-0.11' )" ]]; then
  # Checkout master-0.11
  git branch -d master-0.11 || true
  git fetch origin master-0.11:master-0.11
  git checkout master-0.11
fi

bump.sh ${VERSION} > ${VERSION_FILE}
git add ${VERSION_FILE}
git commit -m "Incremented version from ${VERSION} to $( cat ${VERSION_FILE} )"
git push origin master-0.11
