terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-ecs-bastion-simple"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

module "bastion" {
  source = "../../"


  aws_region = "eu-west-1"
  name       = "test-bastion"

  vpc_id         = module.vpc.vpc_id
  vpc_cidr_block = module.vpc.vpc_cidr_block

  bastion_encrypt_logs       = true
  bastion_egress_cidr_blocks = "10.0.101.0/24"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "test-bastion"

  cidr = "10.0.0.0/16"

  azs            = ["eu-west-1a"]
  public_subnets = ["10.0.101.0/24"]
}

module "ecs" {
  source  = "terraform-aws-modules/ecs/aws"
  version = "v2.0.0"

  name = "test-bastion"
}

