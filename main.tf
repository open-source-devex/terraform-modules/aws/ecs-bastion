data "aws_iam_policy_document" "ecs_tasks" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_task_execution" {
  name               = "${var.name}-ecs-task-execution"
  assume_role_policy = data.aws_iam_policy_document.ecs_tasks.json
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution" {
  role       = aws_iam_role.ecs_task_execution.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


module "bastion_container_definition" {
  source  = "cloudposse/ecs-container-definition/aws"
  version = "v0.15.0"

  container_name  = "${var.name}-bastion"
  container_image = var.bastion_container_image

  container_cpu                = var.bastion_container_task_cpu
  container_memory             = var.bastion_container_task_memory
  container_memory_reservation = var.bastion_container_memory_reservation

  port_mappings = [
    {
      containerPort = 22
      hostPort      = 22
      protocol      = "tcp"
    },
  ]

  log_options = {
    "awslogs-region"        = var.aws_region
    "awslogs-group"         = aws_cloudwatch_log_group.bastion.name
    "awslogs-stream-prefix" = "${var.name}-ecs-bastion"
  }
}

resource "aws_ecs_task_definition" "bastion" {
  family = "${var.name}-bastion"

  execution_role_arn       = aws_iam_role.ecs_task_execution.arn
  task_role_arn            = aws_iam_role.ecs_task_execution.arn
  network_mode             = var.ecs_network_mode
  requires_compatibilities = [var.ecs_launch_type]
  cpu                      = var.bastion_container_task_cpu
  memory                   = var.bastion_container_task_memory

  container_definitions = module.bastion_container_definition.json

  tags = var.tags
}

module "bastion_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.1.0"

  name        = "${var.name}-ecs-bastion"
  description = "Security group for ECS Bastions of ${var.name}"
  vpc_id      = var.vpc_id

  tags = merge(var.tags, {
    Name = "${var.name}-ecs-bastion"
  })

  ingress_with_cidr_blocks = [
    {
      rule        = "ssh-tcp"
      cidr_blocks = var.bastion_ingress_cidr_blocks
    },
  ]

  egress_with_cidr_blocks = [
    {
      rule        = "ssh-tcp"
      cidr_blocks = var.bastion_egress_cidr_blocks
    },
    {
      # download container image
      rule        = "https-443-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}
